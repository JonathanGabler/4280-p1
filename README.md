# 4280-P1

## Program Details
* Author: Jonathan Gabler
* Language: C++
* Class:  CS4280 Program Translations
* Project: Project 1
* Date:   10/27/2019

## How to run
* Build executable with makefile
* ./scanner filename (where filename is name of test file)

You can find example files to use as test file in TestFiles Folder.

## Notes
Program performs as exepected. 
