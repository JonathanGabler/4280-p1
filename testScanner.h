//-----------------------------------------------------------------------------
//Author:   Jonathan Gabler
//Class:    CS4280 Program Translations
//Assign:   Project 1
//Date:     10/27/19
//File:     testScanner.h
//-----------------------------------------------------------------------------

#ifndef P1_TESTSCANNER_H
#define P1_TESTSCANNER_H

#include <istream>

using namespace std;

//Function prototypes
bool isValidCharacter(char);
int filter(string &);
int testScanner(istream &);

#endif //P1_TESTSCANNER_H
