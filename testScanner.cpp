//-----------------------------------------------------------------------------
//Author:   Jonathan Gabler
//Class:    CS4280 Program Translations
//Assign:   Project 1
//Date:     10/27/19
//File:     testScanner.cpp
//-----------------------------------------------------------------------------

#include "testScanner.h"
#include <iostream>
#include <istream>
#include "token.h"
#include "scanner.h"


//Globals
//Current index within the current line
int current_index;
// Current line index
int current_line_index;
//general flag
//static int comment_flag;
bool comment_flag = false;

string input_file_line;
/// Test the character is valid or not using given grammar in token.h
/// \param character
/// \return boolean
bool isValidCharacter(char character){
    if(!isOp(character) && !isdigit(character) && !isalpha(character)){
        return false;
    }
    else{
        return true;
    }
}

/// Filter the passed in string
/// \param unfiltered_string
/// \return current_index
int filter(string &unfiltered_string){

    //print the unfiltered string to be filtered.
    cout << "Unfiltered string: " << endl << unfiltered_string << endl << endl;

    //String to check overflow of current index line index compared to passed string
    if(current_index >= unfiltered_string.length())
        return 0;
    string filtered_string;

    char past_character;
    char current_character;
    char next_character;
    char SPACE = ' ';

    //loop through the unfiltered string
    for(int i = current_index; i < unfiltered_string.length(); i++){
        current_character = unfiltered_string.at(i);

        if(i > 0){
            //set past char to previous one in string
            past_character = unfiltered_string.at(i-1);
        }

        //check for damn comments. NOT SURE IF THIS PART WORKS. only did sometimes...
        if(current_character == COMMENT){
            filtered_string.push_back(SPACE);

            //If the comment flag is set then turn off because marks end of comment
            //If the comment flag is not set then turn on becuase marks start of comment
            comment_flag = !comment_flag;
        }
        //character is not a comment
        else if(!comment_flag){
            //check if is a space or not
            if(isspace(current_character)){
                if(!isspace(past_character)){
                    filtered_string.push_back(current_character);
                }
            }
            //Not a comment or a space so test if its in our grammar rules
            else if(!isValidCharacter(current_character)){
                //Character is not grammar throw error
                cout << "ERROR: invalid character on line: " << current_line_index << " at character: " << current_index;
                return -1;
            }
            //Not a coment or a space but is a valid character in the grammar
            else{
                filtered_string.push_back(current_character);
            }
        }

        current_index++;
    }
    //set unfiltered passed string to the new filtered string
    input_file_line.assign(filtered_string);
    //Output the filtered string
    cout << "Filtered string to be scanned: " << endl << filtered_string << endl << endl;
    return current_index;
}

/// Driver for the scanner function in scanner.cpp
/// \param in
/// \return
int testScanner(istream &in){

    //Build our maps in token.h
    //buildkeywordMap();
    buildkeywordMap();
    buildOpMap();

    string string_input_line;
    Token token;

    //set the current line index to first line in file
    current_line_index = 1;

    //loop through file
    while(getline(in, input_file_line )){
        //set index to column 1 on FSA table
        current_index = 0;

        //Remove unwanted characters and spaces
        if(filter(input_file_line) == -1 )
            return 1;
        current_token_index = 0;
        //Test if line is empty
        if(input_file_line.length() > 0){
            //pass to scanner
            while(scanner(input_file_line, token)==0)
                //Print token to screen
                tokenDisplay(token);
        }
        //move to next line in file if there is one
        current_line_index++;
    }
    //End of file
    if(in.eof()){
        token.ID  = EOFtk;
        token.tk_Description = "EOF";
        if(current_line_index > 1)
            token.line_Number = current_line_index--;
        else
            token.line_Number = current_line_index;
        tokenDisplay(token);
    }
    return 0;
}
