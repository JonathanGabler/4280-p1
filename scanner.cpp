//-----------------------------------------------------------------------------
//Author:   Jonathan Gabler
//Class:    CS4280 Program Translations
//Assign:   Project 1
//Date:     10/27/19
//File:     scanner.cpp
//-----------------------------------------------------------------------------

#include <iostream>
#include <string>
#include <cctype>
#include "scanner.h"
#include "token.h"

using namespace std;

/// Test the passed char to match it up with the correct column in state table
/// \param character
/// \return
int getColumn(char character){
    //Is char a letter?
    if(isalpha(character)){
        //Is char a lowercase?
        if(islower(character))
            return 0;
        else
            return 1;
    }
    else if(isdigit(character))
        return 2;
    else if(isOp(character))
        return 3;
    else if(isspace(character))
        return 4;
    else {
        cout << "When matching char to column no match was found. Character tested was: " << character;
        return -1;
    }
}

/// throw error to user to let them know scanner failed at what instance
/// \param state
/// \param input_string
void errorOccured(int state, string input_string){
    //Print out fatal error when scanning
    cout << "ERROR: invalid character on line: " << current_line_index << " at character: " << current_index;

    if(state == ERROR_DIGIT){
        cout << "ERROR: digit tokens must contain only digits\n";
    }
    else if(state == ERROR_UPPERCASE){
        cout << " ERROR Meaning: Tokens must start with lowercase letter followed by 3 or more upper case, lower case or digit characters\n";
    }
    else
        cout << " ERROR Meaning: No idea what went wrong here to be honest...";

}

int current_token_index;

/// Scanner function
/// \param input_file_string
/// \param token
/// \return
int scanner(string &input_file_string, Token &token){

    //line number
    token.line_Number = current_line_index;

    int current_state = 0;
    // FSA Rows
    int next_state;
    // FSA columns
    int next_input;

    char next_character;
    const char SPACE = ' ';
    string token_description;

    //loop through string to classify tokens one by one.
    while(current_token_index <= input_file_string.length()){
        if(current_token_index < input_file_string.length()){
            next_character = input_file_string.at(current_token_index);
        }
        //END OF STRING REACHED BUT STILL NEEDS COUNTED.
        else
            next_character = SPACE;

        //GET COLUMN
        next_input = getColumn(next_character);
        // Get the row
        next_state = FSA_TABLE[current_state][next_input];

        //FSA returned a value graeter than 1000
        if(next_state > 1000){

            token.tk_Description = token_description;
            //switch to find which final case occured
            switch (next_state){
                case ID_FINAL_STATE:
                    if(isKeyword(token) != -1){
                        token.ID = KEYWORDtk;
                        token.tk_Description.append(" " + token_description);
                    }
                    else{
                        token.ID = IDtk;
                        token.tk_Description.assign("IDtk " + token_description);
                    }
                    break;
                case INT_FINAL_SATE:
                    token.ID = INTtk;
                    token.tk_Description.assign("INTtk " + token_description);
                    break;
                case OP_FINAL_STATE:
                    token.ID = OPtk;
                    token.tk_Description.assign("OPtk " + token_description);
                    break;
            }

            return 0;
        }
        // If FSA table returns a negative number
        else if(next_state < 0){
            //reset
            current_state = 0;
            //call errorOccured to display error
            errorOccured(next_state, input_file_string);
            //Move to next token
            current_token_index++;
            //Abandon ship
            return -1;
        }

        current_state = next_state;
        current_token_index++;

        if(!isspace(next_character))
            token_description.push_back(next_character);
    }
    //If you reach this point some serouis shit went wrong
    return -1;

}
