//-----------------------------------------------------------------------------
//Author:   Jonathan Gabler
//Class:    CS4280 Program Translations
//Assign:   Project 1
//Date:     10/27/19
//File:     main.cpp
//-----------------------------------------------------------------------------

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include "token.h"
#include "testScanner.h"
#include "scanner.h"



using namespace std;

///
/// \param argc - input arguments
/// \param argv - array of input arguments
/// \return
int main(int argc, char* argv[]){

    //set file stream to input file
    ifstream input_file;

    //name of file
    char * input_file_name;

    //check for arguments given
    //The user should enter two arguments [scanner, "filename"]
    if (argc < 2){
        cout << "ERROR: TOO FEW ARGUMENTS PROVIDED. Please follow the below format.";
        cout << "scanner filename \n Where filename is the file you wish to test.";
        return -1;
    }
    else if(argc > 2){
        cout << "ERROR: TOO MANY ARGUMENTS PROVIDED. Please follow the below format.";
        cout << "scanner filename \n Where filename is the file you wish to test.";
        return -1;
    }
    else if(argc == 2){
        // file_name = user input for filename
        string file_name = argv[1];

        //Copy the contents of the file to a new copy to maintain the orginal one
        input_file_name = new char[file_name.length() + 1];
        strcpy(input_file_name,file_name.c_str());

        //Open the new file created
        input_file.open(input_file_name, ifstream::in);

        //Check if the file was opened, if not error out
        if(!input_file.is_open()){
            cout<<"ERROR: The file" << input_file_name << " could not be opened. Terminating.....\n";
            return -1;
        }
        else{
            //Let the user know the file is open
            cout<<"PROGRESS: The file " << input_file_name << " has been opened. Continuing.....\n";
            // Call the test scanner
            testScanner(input_file);

        }
    }
    //Close the file
    input_file.close();

    //Terminate program on completion
    return 0;
}